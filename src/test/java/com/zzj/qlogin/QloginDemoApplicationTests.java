package com.zzj.qlogin;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import javax.annotation.Resource;

@SpringBootTest
class QloginDemoApplicationTests {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Test
    void contextLoads() {


        stringRedisTemplate.opsForValue().set("a","CCC");
        Object a = stringRedisTemplate.opsForValue().get("a");
        System.out.println(a);
    }

}
