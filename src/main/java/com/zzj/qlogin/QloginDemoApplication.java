package com.zzj.qlogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QloginDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(QloginDemoApplication.class, args);
    }

}
