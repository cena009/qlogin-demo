package com.zzj.qlogin.service;

import java.util.Map;

public interface WxService {

    String getAccessToken();
    Map<String,Object> getQrCode();
}
